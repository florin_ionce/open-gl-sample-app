#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/glm.h>
#include <tga.h>

#include <iostream>
#include <fstream>
using namespace std;

int screen_width=640;
int screen_height=480;




GLfloat mylight_ambient[] = {0.1, 0.1, 0.1, 0.1};
GLfloat material_emission[] = {1.0, 1.0, 0.0, 1.0};
GLfloat mylight_position[] = {1, 1, 0, 1};

// angle of rotation for the camera direction
float angle=0.0;
// actual vector representing the camera's direction
float lx=0.0f,lz=-1.0f;
// XZ position of the camera
float x=0.0f,z=5.0f;


const GLfloat color[3] ={0.7,0.7,0.7};
float fogD = 0.003;
int enableFog = 0;
int enableRain = 0;

GLMmodel *table, *watercan, *sceneObj, *sanie, *snowman;

GLuint skyboxTexture[6]; //variable used for the IDs of the six textures that form the skybox
GLfloat fSkyboxSizeX, fSkyboxSizeY, fSkyboxSizeZ; //skybox size on X, Y and Z axes
GLfloat fTreeSize;

//textures

GLuint redTexture, sceneTexture, sanieT;

void renderObjects();
void renderFog();


void drawModel(GLMmodel **pmodel, char *filename, GLuint mode)
{
    if(!*pmodel)
    {
        *pmodel=glmReadOBJ(filename);
        if(!*pmodel)exit(0);
        glmUnitize(*pmodel);
        //generate facet normal vectorsfor model
        glmFacetNormals(*pmodel);
        //generate vertex normalvectors (called after generating facet normals)
        glmVertexNormals(*pmodel,90.0);
    }
    glmDraw(*pmodel,mode);
}

void initOpenGL()
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);
    glViewport(0, 0, screen_width, screen_height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)screen_width/(GLfloat)screen_height, 1.0f, 1000.0f);
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glMatrixMode(GL_MODELVIEW);



    //generate new texture IDs and load each texture that will be used
    glGenTextures(1,&redTexture);
    loadTGA("assets/Textures/test.tga",redTexture);

    glGenTextures(1,&sceneTexture);
    loadTGA("assets/Textures/cub_mare.tga",sceneTexture);

    glGenTextures(1,&sanieT);
    loadTGA("assets/Textures/sanie.tga",sanieT);

    	//set skybox size
	fSkyboxSizeX =200.0;
	fSkyboxSizeY =30.0;
	fSkyboxSizeZ =200.0;

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHT1);
        glEnable(GL_NORMALIZE);
}


void DrawSkybox (GLfloat sizeX, GLfloat sizeY, GLfloat sizeZ)
{
	glEnable(GL_TEXTURE_2D); //enable 2D texturing


	//////////////////////////////////////////////////////////////////
	// please consult the orientation convention for this skybox    //
	// ("orientation_convention.png" file in the "Textures" folder) //
	//////////////////////////////////////////////////////////////////


	//negative x plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[0]); //select the current texture
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(-sizeX, sizeY, -sizeZ); //assign each corner of the texture to a 3D vertex in the OpenGL scene
		glTexCoord2f(0, 1);glVertex3f(-sizeX, sizeY, sizeZ); //(0,0) is the left lower corner, while (1,1) is the right upper corner of the texture
		glTexCoord2f(0, 0);glVertex3f(-sizeX, -sizeY, sizeZ);
		glTexCoord2f(1, 0);glVertex3f(-sizeX, -sizeY, -sizeZ);
	glEnd();

	//negative y plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[1]);
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(sizeX, -sizeY, -sizeZ);
		glTexCoord2f(0, 1);glVertex3f(-sizeX, -sizeY, -sizeZ);
		glTexCoord2f(0, 0);glVertex3f(-sizeX, -sizeY, sizeZ);
		glTexCoord2f(1, 0);glVertex3f(sizeX, -sizeY, sizeZ);
	glEnd();

	//negative z plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[2]);
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(-sizeX, sizeY, sizeZ);
		glTexCoord2f(0, 1);glVertex3f(sizeX, sizeY, sizeZ);
		glTexCoord2f(0, 0);glVertex3f(sizeX, -sizeY, sizeZ);
		glTexCoord2f(1, 0);glVertex3f(-sizeX, -sizeY, sizeZ);
	glEnd();

	//positive x plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[3]);
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(sizeX, sizeY, sizeZ);
		glTexCoord2f(0, 1);glVertex3f(sizeX, sizeY, -sizeZ);
		glTexCoord2f(0, 0);glVertex3f(sizeX, -sizeY, -sizeZ);
		glTexCoord2f(1, 0);glVertex3f(sizeX, -sizeY, sizeZ);
	glEnd();

	//positive y plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[4]);
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(sizeX, sizeY, sizeZ);
		glTexCoord2f(0, 1);glVertex3f(-sizeX, sizeY, sizeZ);
		glTexCoord2f(0, 0);glVertex3f(-sizeX, sizeY, -sizeZ);
		glTexCoord2f(1, 0);glVertex3f(sizeX, sizeY, -sizeZ);
	glEnd();

	//positive z plane
	glBindTexture(GL_TEXTURE_2D, skyboxTexture[5]);
	glBegin(GL_QUADS);
		glTexCoord2f(1, 1);glVertex3f(sizeX, sizeY, -sizeZ);
		glTexCoord2f(0, 1);glVertex3f(-sizeX, sizeY, -sizeZ);
		glTexCoord2f(0, 0);glVertex3f(-sizeX, -sizeY, -sizeZ);
		glTexCoord2f(1, 0);glVertex3f(sizeX, -sizeY, -sizeZ);
	glEnd();

	glDisable(GL_TEXTURE_2D); //disable 2D texuring
}


void drawSnowMan()
{

    glColor3f(1.0f, 1.0f, 1.0f);

// Draw Body
    glTranslatef(0.0f ,0.75f, 0.0f);
    glutSolidSphere(0.75f,20,20);

// Draw Head
    glTranslatef(0.0f, 1.0f, 0.0f);
    glutSolidSphere(0.25f,20,20);

// Draw Eyes
    glPushMatrix();
    glColor3f(0.0f,0.0f,0.0f);
    glTranslatef(0.05f, 0.10f, 0.18f);
    glutSolidSphere(0.05f,10,10);
    glTranslatef(-0.1f, 0.0f, 0.0f);
    glutSolidSphere(0.05f,10,10);
    glPopMatrix();

// Draw Nose
    glColor3f(1.0f, 0.5f , 0.5f);
    glutSolidCone(0.08f,0.5f,10,2);
}

void renderScene(void){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(x, 1.0f, z, x+lx, 1.0f,  z+lz, 0.0f, 1.0f,  0.0f);

    GLfloat ambientColor[] = {3.0f,3.0f,3.0f,1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

    glPushMatrix();
    glTranslatef(0, 10, 0);
                 glLightfv(GL_LIGHT0, GL_AMBIENT, mylight_ambient);
                 glLightfv(GL_LIGHT0, GL_POSITION, mylight_position);
    glPopMatrix();

    renderObjects();

    // Draw 36 SnowMen
    for(int i = -3; i < 3; i++)
        for(int j=-3; j < 3; j++)
        {
            glPushMatrix();
            glTranslatef(i*10.0,1,j * 10.0);
            drawModel(&snowman,"assets/Objects/snowman.obj",GLM_NONE|GLM_COLOR);
            glPopMatrix();
        }
    renderFog();

    glutSwapBuffers();
}



void changeSize(int w, int h)
{
    screen_width=w;
    screen_height=h;

    if(h == 0)
        h = 1;

    float ratio = 1.0*w/h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(45.0f, ratio, 1.0f, 1000.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

}

int toggle(int x){
    if(x == 0)
        return 1;
    if(x == 1)
        return 0;
}

void processNormalKeys(unsigned char key, int x, int y)
{
    static int wireframeOption;

    switch(key)
    {
    case 't':
        //process
        glutPostRedisplay();
        break;

    case 'f':
        enableFog = toggle(enableFog);
        break;

    case 'p':
        wireframeOption = wireframeOption == 0 ? 1 : 0;
                if(wireframeOption)
                        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                else
                        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                glutPostRedisplay();
                break;

    }


}
float y =0.0f;
void processSpecialKeys(int key, int xx, int yy)
{

    float fraction = 0.5f;

    switch (key)
    {
    case GLUT_KEY_LEFT :
        angle -= 0.01f;
        lx = sin(angle);
        lz = -cos(angle);
        y -= 0.8f;
        break;
    case GLUT_KEY_RIGHT :
        angle += 0.01f;
        lx = sin(angle);
        lz = -cos(angle);
        y += 0.8f;
        break;
    case GLUT_KEY_UP :
        x += lx * fraction;
        z += lz * fraction;
        break;
    case GLUT_KEY_DOWN :
        x -= lx * fraction;
        z -= lz * fraction;
        break;
    }
}



int main(int argc, char* argv[])
{

    //Initialize the GLUT library
    glutInit(&argc, argv);
    //Set the display mode
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    //Set the initial position and dimensions of the window
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(screen_width, screen_height);
    //creates the window
    glutCreateWindow("First OpenGL Application");
    //Specifies the function to call when the window needs to be redisplayed
    glutDisplayFunc(renderScene);
    //Sets the idle callback function
    glutIdleFunc(renderScene);
    //Sets the reshape callback function
    glutReshapeFunc(changeSize);
    //Keyboard callback function
    glutKeyboardFunc(processNormalKeys);

    glutSpecialFunc(processSpecialKeys);
    //Initialize some OpenGL parameters
    initOpenGL();
    //Starts the GLUT infinite loop
    glutMainLoop();


    return 0;
}


void renderObjects(){

    glPushMatrix();

            glRotatef(180, 1, 0, 0);
            glScalef(400.0f, 30.0f, 400.0f);
            glTranslatef(0,-1,0);
            glEnable(GL_TEXTURE_2D); //enable 2D texturing
            glBindTexture(GL_TEXTURE_2D, sceneTexture); //select the current texture

            glEnable(GL_ALPHA_TEST); //enable alpha testing
            glAlphaFunc(GL_GREATER, 0.1f); //select the alpha testing function

            //draw the object
            drawModel(&sceneObj,"assets/Objects/cub_mare.obj",GLM_NONE|GLM_TEXTURE);


            glDisable(GL_ALPHA_TEST); //disable alpha testing
            glDisable(GL_TEXTURE_2D); //disable 2D texturing

            glPopMatrix();


        glPushMatrix();

            glEnable(GL_TEXTURE_2D); //enable 2D texturing
            glBindTexture(GL_TEXTURE_2D, sanieT); //select the current texture

            glEnable(GL_ALPHA_TEST); //enable alpha testing
            glAlphaFunc(GL_GREATER, 0.1f); //select the alpha testing function



              glTranslatef(x,0.5,z-1);
              glRotatef(y,0,-1,0);

            //draw the object
            drawModel(&sanie,"assets/Objects/sanie.obj",GLM_NONE|GLM_TEXTURE);


            glDisable(GL_ALPHA_TEST); //disable alpha testing
            glDisable(GL_TEXTURE_2D); //disable 2D texturing

    glPopMatrix();


 for(int i = -3; i < 3; i++)
        for(int j=-3; j < 3; j++)
        {
            if((i + j)%3 ==0){
            glPushMatrix();
            glTranslatef(i*10.0,0,j * 10.0);

            glEnable(GL_TEXTURE_2D); //enable 2D texturing
            glBindTexture(GL_TEXTURE_2D, redTexture); //select the current texture

            glEnable(GL_ALPHA_TEST); //enable alpha testing
            glAlphaFunc(GL_GREATER, 0.1f); //select the alpha testing function


            glTranslatef(0.0f ,0.75f, -2.5f);
            glScalef(0.2f,0.2f,0.2f);
            //draw the object
            drawModel(&watercan,"assets/Objects/test.obj",GLM_NONE|GLM_TEXTURE);


            glDisable(GL_ALPHA_TEST); //disable alpha testing
            glDisable(GL_TEXTURE_2D); //disable 2D texturing

            glPopMatrix();
            }
        }
}


void renderFog(){
    if(enableFog == 1){
        glEnable (GL_DEPTH_TEST);
        glEnable (GL_FOG);
        glFogi (GL_FOG_MODE, GL_EXP2);
        glFogfv (GL_FOG_COLOR, color);
        glFogf (GL_FOG_DENSITY, fogD);
        glHint (GL_FOG_HINT, GL_NICEST);
    }else if(enableFog == 0){
        glDisable(GL_FOG);
    }
}
